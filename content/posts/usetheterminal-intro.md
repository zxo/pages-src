---
title: "Use The Terminal on Linux: Intro"
date: 2021-07-01T19:23:47-06:00
tags:
- terminal
- tutorial
- linux
---

The terminal is a great tool for Linux, but most people who use Linux have no clue how to use it. It is really easy once you get used to it, just type a command and get it done. Here's a simple guide on the basics of the terminal.

## The basics before commands

#### Root:

*Root* here is used to refer to system files outside of the user's directory.  
Basically, each user has a home directory, which is contained in a folder separate from the system files.

This makes sure users without administrator access can not modify the system files, only install stuff to the user directory.  
This also reduces the chance of malware taking over the system.  

Sometimes, operating systems do not come with root access to any user. Two examples are most Android systems (for phones), and Fedora Silverblue (for desktop).   
These types of operating systems are referred to *immutable* operating systems.

#### Linux distributions:

*Linux distributions*, commonly called *distros* in the Linux community, are effectively different operating systems, all using Linux.   
They operate in a fork fashion, where operating systems fork off of another one. These operations systems are called *forks*, and most Linux operating systems are forks of something else.   

How it works:   
You have the starting GNU operating system. Fedora, Arch Linux, Debian, and Slackware are forked off of the GNU OS.   
Ubuntu, Tails OS, Kali Linux, and PureOS are based off of Debian.   
Elementary OS, Linux Mint, Zorin OS, and Pop!_OS are forks of Ubuntu.

There are many more Linux operating systems out there that are not shown here, such as QubesOS and Red Hat Enterprise Linux (RHEL) that are forked from Fedora. There are over 1000 Linux distros so there are undoubtedly many. many more than I listed.

Distros control file management in some form and affect how to terminal works.

## Starting up the terminal

The terminal icon normally has an icon with a black box with `>_`, `> `, `>.`, or something similar located inside of it. A visual representation looks as so:

![GNOME Terminal logo](https://codeberg.org/zxo/pages-src/raw/commit/7ee7432e4be15f382f9015d21a4b9de1cf16ba13/assets/term.png)

When you open the terminal, you'll see something like this:

`[user@computer ~]$`

You have successfully started the terminal, it is ready to be used.

We will be learning how to manage packages and processes today through the terminal.

## Installing packages through the terminal

### Search for packages

Say you want to install something that looks like the matrix. Maybe let's search it.

In terms of commands, enter as such to search for matrix:

Fedora:

`[user@computer ~]$ dnf search matrix`

Manjaro:

`[user@computer ~]$ pamac search matrix`

Anything else:

`[user@computer ~]$ apt search matrix`

_\*_ *Please note that there are multiple distros that do not use these commands, but it is likely that if you are using them you'll know how to manage packages on them.*

### Installing packages

Cmatrix looks like a good tool to do the job for this.   
Now it's time to install it.

But before you install it, we need to grant root access in order for the program to work.

This can be done using the command `sudo`.

After running a command with `sudo`, you have to enter the administrator password. There is no visual confirmation so watch out.

Fedora:

`[user@computer ~]$ sudo dnf install cmatrix`

Manjaro:

`[user@computer ~]$ sudo pamac install cmatrix`

Anything else:

`[user@computer ~]$ sudo apt install cmatrix`

_\*_ *Remember the password for sudo -  this is important.*

It will also install all the dependencies so it won't break.

### Root terminal

If you need to execute more than one terminal command in a row, use of of these two following commands:

`[user@computer ~]$ sudo su`

`[user@computer ~]$ sudo -s`

These will change the terminal into a root terminal, which starts with a hashtag:

`[root@computer user]#`

When you run a command from here, imagine `sudo` is placed in front of the command.

### Uninstalling packages.

Maybe you don't like it, lets uninstall it.

Fedora:

`[user@computer ~]$ sudo dnf remove cmatrix`  
or  
`[root@computer user]# dnf remove cmatrix`

OR

`[user@computer ~]$ sudo dnf erase cmatrix`  
or  
`[root@computer user]# dnf erase cmatrix`

Manjaro:

`[user@computer ~]$ sudo pamac remove cmatrix`  
or  
`[root@computer user]# pamac remove cmatrix`

Anything else:

`[user@computer ~]$ sudo apt remove cmatrix`  
or  
`[root@computer user]# apt remove cmatrix`

OR

`[user@computer ~]$ sudo apt purge cmatrix`  
or  
`[root@computer user]# apt purge cmatrix`

_\*_ *The difference between remove and erase/purge is that remove just removes the file, erase/purge removes the config files so if an app breaks on reinstall try using erase/purge.*

### Updates

Updates are super easy with the terminal! Just run this command:

Fedora:

`[user@computer ~]$ sudo dnf update`  
or   
`[root@computer user]# dnf update`

Manjaro:

`[user@computer ~]$ sudo pamac update`   
or   
`[root@computer user]# pamac update`

Anything else:

`[user@computer ~]$ sudo apt update`   
OR   
`[root@computer user]# apt update`

## Process management

### Starting processes

Starting processes with the terminal is quite simple. However, you have to know the process name.   
For example, Files on GNOME is `nautilus`, on KDE the file manager is `dolphin`, and more.   
If you just run a command with the name of the program it will open the program.

For example:

`[user@computer ~]$ firefox` will open Firefox.

`[user@computer ~]$ geany` will open Geany

`[user@computer ~]$ emacs` will open Emacs.

Sometimes, the program will make it so that the terminal will not be able to be used until the program is closed.   
To circumvent this, add an ampersand at the end of the command.

`[user@computer ~]$ firefox &` will open Firefox in the background so you can use the terminal even with Firefox open.

`[user@computer ~]$ geany &` Much of the same as Firefox.

`[user@computer ~]$ emacs &` You get the idea.

_\*_ *Much like `sudo su`, running a process is much the same on any distro.*

#### Running processes with root

Sometimes you want to open programs with root. Like files, to access system files. In which case you would do

`[user@computer ~]$ sudo nautilus`   
or   
`[root@computer user]# nautilus`

OR

`[user@computer ~]$ sudo nautilus &`   
or   
`[root@computer user]# nautilus &`

to open GNOME Files. Or replace `nautilus` to open any other program with root.   
Remember, you need password confirmation.

### Killing processes

You could also call it ending processes.

To do it, enter `pkill` followed by the process name. It will kill the process, for stuff such as `gnome-software` that stay open in the backgroud when you close them, or for unresponsive processes.

`[user@computer ~]$ pkill gnome-software` would kill GNOME Software

`[user@computer ~]$ pkill session` would kill GNOME Sesion, not this logs you out.

`[user@computer ~]$ pkill dolphin` would kill KDE files.

And you get the idea.

## Exit

Now you know how to use the terminal for a few things. Hope it helps you out!