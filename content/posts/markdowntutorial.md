---
title: "Markdown Tutorial"
date: 2021-06-26T14:34:19-06:00
tags:
- markdown
- tutorial
- md
---

Have you ever been using Microsoft Word, google Docs, LibreOffice Writer, whatever you use to write documents and been overwhelmed with how many buttons, options, fonts, and stuff it has? 

Have you ever wanted something much simpler that is easy to write in with simple formatting, without any buttons, just text? Then try Markdown, probably one of the simplest text editors out there. 

It is very simple, but yet also quite powerful. It has no frills, just the basics. It is great for writing documents, notes, comments on social media, and everywhere in between.

## What even&nbsp;*is*&nbsp;Markdown?

Markdown is a text editor that is both simple and powerful. It is different from a [WYSIWYG](https://en.wikipedia.org/wiki/WYSIWYG) text editor in that Markdown does not look like its end result: it has syntax, like code.

However, Markdown syntax is easy to read, so even if you don't see the end result you can picture it without advanced technological knowledge.

It is very simple, and is supported by mostly everything. It is not fancy, it is very simple, to keep the user experience nice.

Something to note about Markdown is the syntax is sensitive, so `#Header 1` won't render a header, while `# Header 1` will.

## Markdown Syntax

### Headers

There are six headers in Markdown, and you normally write them with hashtags in front.

``` md
# Header 1
 
## Header 2

### Header 3

#### Header 4

##### Header 5

###### Header 6
```

Which renders as:

***
# Header 1

## Header 2

### Header 3

#### Header 4

##### Header 5

###### Header 6
***

I normally use header 1 as title, header 2 as section titles, header 3 as sub-section titles, etc.

## Normal text

### Breaks

Normal text you can find by typing normally. To go down a line, leave a line of blank space between two paragraphs.

``` md
This will not
do anything.

But this will.
```


will render as:
 

***
This will not
do anything.

But this will.
***

If you do want to break, end the line with two or more spaces, and enter once:

``` md
No break
here. But I can break with two spaces  
which is cool.
```

renders as:

***
No break
here. But I can break with two spaces  
which is cool.
***

### Paragraph markup

In Markdown, you can *italicize* text, **bold** it, and ***do both at once***. 

You italicizs text surrounding it with one asterisk or underscore, bold it with two, and do both with three.

``` md
**Markdown** is *really cool*, because it is _super_ simple and __easy__.

Markdown is ***probably*** my favorite text editor in this ___world___. 
It is soo*oo*oo**oo**oo***oo*** great!
```

shows up as:

***
**Markdown** is *really cool*, because it is _super_ simple and __easy__.

Markdown is ***probably*** my favorite text editor in this ___world___. 
It is soo*oo*oo**oo**oo***oo*** great!
***

Note: Do not use underscores in the middle of words, it might not work.

``` md
"Loo_oooo_ok!"
```

shows as:

***
"Loo_oooo_ok!"
***

It's best to stick with asterisks here.

## Lists

Markdown supports lists of two types, numbered and bulleted.

### Numbered Lists

Ordered lists are numbered lists. It doesn't matter what numbers you put in as long as the first one is a 1.

``` md
1. Thing
2. Thing
3. Thing
	1. Sub-thing
	2. Sub-thing
4. Thing

Markdown is cool.

1. Thing
8. Thing
2915. Thing
	1. Sub-thing
	52. Sub-thing
6. Thing
```

Writing this renders as such in Markdown:

***
1. Thing
2. Thing
3. Thing
	1. Sub-thing
	2. Sub-thing
4. Thing

Markdown is cool.

1. Thing
8. Thing
2915. Thing
		1. Sub-thing
		52. Sub-thing
6. Thing
***

### Bulleted lists

You write bulleted lists with either -, *, or +.

Writing this:

``` md
- List
- of
- things
	- sub-thing.

Cool.

+ List
+ of
+ things
	+ sub-thing.

Again?

* List
* of 
* things
	* sub-thing.
```

would show up like:

***
- List
- of
- things
	- sub-thing.

Cool.

+ List
+ of
+ things
	+ sub-thing.

Again?

* List
* of 
* things
	* sub-thing.
***

## Blocks

There are again, two types of blocks, block quotes and code blocks.

### Block quotes.

Block quotes are written by starting the line with a >. They can span multiple lines, and nest.

``` md
> ### This is my block quote.
>
> It is cool, I like it.
>
> - And it can
>
>> nest in
>>
>>> *and further in*
>>>
>> back out
>>
> and back to here.
>
```

***
> ### This is my block quote.
> 
> It is cool, I like it.
>
> - And it can
>
>> nest in
>>
>>> *and further in*
>>>
>> back out
>>
> and back to here.
>
***

### Code blocks

Code blocks cone in two forms (twoception, am I right?) again: single-line and multi line.

#### Single-line code blocks

Single-line code blocks are made by surronding a word with a backtick (`) on each side.

``` md
Today, I will look at three shells for Linux: 
`Bash`, `Dash`, and `Fish`.

They all are able to run `sudo firefox`.
```

***
Today, I will look at three shells for Linux: 
`Bash`, `Dash`, and `Fish`.

They all are able to run `sudo firefox`.
***

#### Multi-line code blocks

Multi-line code blocks can be made with three ``` on seperate lines before and after the code block. Don't put the backslash like I do, I just did it so my code snippet wouldn't break.

``` md
Look at me!
```\
This spans *multiple* lines.

## My Markdown syntax doesn't work!
```\

And now it **does**.
```

***
Look at me!
```
This spans *multiple* lines.

## My Markdown syntax doesn't work!
```

And now it **does**.

***

You can also use code blocks tabbing in once, or twice inside a list, every line of the block.

```
I didn't use tab here

	But I did here and ended with two spaces  
	So I got a break

- And here in a list

		Tabbed in twice for code
		
		In between lines too

- Back to the list.
```

***
I didn't use tab here

	But I did here and ended with two spaces  
	So I got a break

- And here in a list

		Tabbed in twice for code
		
		In between lines too

- Back to the list.
***

### Images

Images are formatted with an exclamation mark at the start, the hover text in brackets, and the image directory in parentheses, either a website or saved onto the device (the server hosting the site if the Markdown file is published to the site).

``` md
Nix OS has a cool logo, and is a cool operating system. 

Just look at it, it is so pretty.

![Nix OS logo](https://codeberg.org/zxo/pages-src/raw/branch/main/assets/nixos.png)
```

***
Nix OS has a cool logo, and is a cool operating system. 

Just look at it, it is so pretty.

![Nix OS logo](https://codeberg.org/zxo/pages-src/raw/branch/main/assets/nixos.png)
***

### Links and URLs

#### Links

Links can be made like images, minus the ! at the start.

``` md
## Invidious

Invidious is a frontend for YouTube that does not need JavaScript.

It has multiple instances, you can access them [here](https://redirect.invidious.io/).
```

***
## Invidious

Invidious is a frontend for YouTube that does not need JavaScript.

It has multiple instances, you can access them [here](https://redirect.invidious.io/).
***

#### URLs (and email adresses)

URLs can be contained within <>, and so can email adresses.

``` md
<https://zxo.codeberg.page> is created by me, zxo.

If you want to reach out to me email me at <zxoclm@protonmail.com>.
```

***
<https://zxo.codeberg.page> is created by me, zxo.

If you want to reach out to me email me at <zxoclm@protonmail.com>.
***

### Escaping

Sometimes you want to use *, -, +, `, or something else without it doing something. Enter escaping - through \ before the character.

``` md
* This makes a *list*.

* Wait, it isn't \*italic*?

\* And no more list for me? That's very \*sad*.
```

***
* This makes a *list*.

* Wait, it isn't \*italic*?

\* And no more list for me? That's very \*sad*.
***

## Review

Markdown is a cool writing format. It is saved in the .md file type (README.md, among other things), and is used when you need something simple to get the job done. I highly recommend it for documents, READMEs, and more, and I find after a while it almost becomes second nature to type in Markdown.

Have a great day, and I hope you know how to use Markdown now if you didn't before!